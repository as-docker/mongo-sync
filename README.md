Docker container that synchronize specified data volumen with s3 bucket using [s3cmd sync](http://s3tools.org/s3cmd-sync) and cron.
If the S3_RESTORE_PATH is empty, script will fetch data from s3 to local destination, however if S3_RESTORE_PATH already contains data script will skip fetching s3 data.

### Usage

    docker run -d [OPTIONS] amsdard/s3-sync

### Parameters:

* `-e ACCESS_KEY=<AWS_KEY>`: Your AWS key.
* `-e SECRET_KEY=<AWS_SECRET>`: Your AWS secret.
* `-e S3_PATH=s3://<BUCKET_NAME>/<PATH>/`: S3 Bucket name and path. Should end with trailing slash.
* `-e S3_RESTORE_PATH=s3://<BUCKET_NAME>/<PATH>/`: S3 path from which we want to restore our data, defaults to S3_PATH Should end with trailing slash.
* `-e DATA_PATH=/<path to data>/`: container's data folder. Should end with trailing slash.

### Optional parameters:

* `-e PARAMS="--dry-run"`: parameters to pass to the sync command ([full list here](http://s3tools.org/usage)).
* `-e 'CRON_SCHEDULE=0 1 * * *'`: specifies when cron job starts ([details](http://en.wikipedia.org/wiki/Cron)). Default is `0 1 * * *` (runs every day at 1:00 am).

### Examples:

Run upload to S3 everyday at 12:00pm:

    docker run -d \
        -e ACCESS_KEY=myawskey \
        -e SECRET_KEY=myawssecret \
        -e S3_PATH=s3://my-bucket/backup/ \
        -e 'CRON_SCHEDULE=0 12 * * *' \
        -v /home/user/data:/data:ro \
        amsdard/s3-sync

