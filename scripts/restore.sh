#!/bin/bash

set -e
S3CMD=/usr/local/bin/s3cmd
LATEST="$DATA_PATH/latest.tar.gz"
DUMP=/tmp/dump

echo "$(date): Downloading backup from s3."
echo "$S3CMD get --recursive --skip-existing ${S3_PATH}${DATA_PATH}/latest.tar.gz ${DATA_PATH}"
$S3CMD get --recursive --skip-existing ${S3_PATH}${DATA_PATH}/latest.tar.gz ${DATA_PATH}

echo "$(date): Extracting backup."
echo `ls -la $DATA_PATH`

mkdir -p $DUMP
tar zxf $LATEST -C $DUMP

echo "$(date): Restoring backup."
if [ $MONGO_AUTH == "no" ];then
  mongorestore -h $MONGO_HOST -p $MONGO_PORT $DUMP
else
  mongorestore -h $MONGO_HOST -p $MONGO_PORT -u $MONGO_USERNAME -p $MONGO_PASSWORD $DUMP
fi

echo "$(date): Cleaning up."
rm -rf $DUMP

echo "$(date): Job finished."
