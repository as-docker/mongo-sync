#!/bin/bash

cd $DATA_PATH

DATE=$(date +%Y%m%d_%H%M%S)
FILENAME="$DATE.tar.gz"
OUT="$DATA_PATH/$FILENAME"
LATEST="$DATA_PATH/latest.tar.gz"

echo "$DATE [Step 1/3] Running mongodump"
if [ $MONGO_AUTH == "no" ];then
  echo "mongodump -h $MONGO_HOST -p $MONGO_PORT --db $MONGO_DB_NAME --out /dump"
  mongodump -h $MONGO_HOST -p $MONGO_PORT --db $MONGO_DB_NAME --out /dump
else
  echo "mongodump -h $MONGO_HOST -p $MONGO_PORT -u $MONGO_USERNAME -p $MONGO_PASSWORD --db $MONGO_DB_NAME --out /dump"
  mongodump -h $MONGO_HOST -p $MONGO_PORT -u $MONGO_USERNAME -p $MONGO_PASSWORD --db $MONGO_DB_NAME --out /dump
fi

echo "$DATE [Step 2/3] Creating tar archive"
cd /dump && tar -zcf $OUT .
cp $OUT $LATEST

echo "$DATE [Step 3/3] Uploading archive to S3"
/usr/local/bin/s3cmd sync $PARAMS "$DATA_PATH" "$S3_PATH/"

echo "$DATE Cleaning up"
rm -rf /dump
find $DATA_PATH -mtime +$BACKUP_RETENTION_DAYS | xargs rm -Rf

echo "$DATE Mongo backup completed successfully"
