status:
	docker ps

image:
	docker build -t amsdard/mongo-sync .

tag:
	docker build -t amsdard/mongo-sync:$(tag) .

push:
	docker push amsdard/mongo-sync:$(tag)

build:
	docker-compose build

up:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

rm:
	docker-compose rm -f

clean:
	docker stop $$(docker ps -q); \
	docker rm $$(docker ps -a -q) && \
	docker rmi $$(docker images -q)

encrypt_vault:
	tar -cf vault.tar vault && \
	openssl aes-128-cbc -in vault.tar -out vault.aes -k $(pass)  && \
	rm vault.tar

decrypt_vault:
	openssl aes-128-cbc -d -in vault.aes -out vault.tar -k $(pass) && \
	tar -xf vault.tar && \
	rm vault.tar
