FROM mongo
MAINTAINER Tomasz Rzany <tomasz.rzany@amsterdam-standard.pl>

RUN apt-get update && \
    apt-get install -y python python-pip cron && \
    rm -rf /var/lib/apt/lists/*

RUN pip install s3cmd

ADD s3cfg /root/.s3cfg


ENTRYPOINT ["/opt/scripts/docker-entrypoint.sh"]
CMD [""]

ADD scripts /opt/scripts
RUN chmod +x /opt/scripts/*
